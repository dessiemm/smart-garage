export default {
    vin: (value) => typeof value === 'string' && value.length === 17,
    registration_plate: (value) => typeof value === 'string' && value.length === 8,
    year: (value) => typeof value === 'string' && value.length === 4,
};