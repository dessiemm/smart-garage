export default {
    email: (value) => value && typeof value === 'string' && value.length > 0 && value.length < 50,
    password: (value) => value && typeof value === 'string' && value.length >= 8,
};