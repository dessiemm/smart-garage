export default {
    firstname: (value) => value && typeof value === 'string' && value.length > 0,
    lastname: (value) => value && typeof value === 'string' && value.length > 0,
    email: (value) => value && typeof value === 'string' && value.length > 0 && value.length < 50,
    phone: (value) => value && typeof value === 'string' && value.length === 10,
};