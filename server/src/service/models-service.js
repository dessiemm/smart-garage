// import brandsData from '../data/brands-data.js';
import getAllModelsByBrandId from '../data/models-data.js';
import queryUtil from '../common/validation-query-result.js';

const { isThereQueryError, getQueryResponse } = queryUtil;

const getAllModels = async (id) => {
    const result = await getAllModelsByBrandId(id);
    return getQueryResponse(result, [isThereQueryError]);
};
export default getAllModels;