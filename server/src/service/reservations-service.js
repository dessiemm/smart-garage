import queryUtil from '../common/validation-query-result.js';
import ordersData from '../data/orders-data.js';
import reservationsData from '../data/reservations-data.js';
import createReservation from '../data/reservations-data.js';

const {isThereQueryError, getQueryResponse} = queryUtil;

const getAllReservations = async () => {
    const result = await reservationsData.getAllReservations();

    return result;
};
const createReservationService = async (req) => {
    const result = await reservationsData.createReservation(req);
    return getQueryResponse(result, [isThereQueryError]);
};
export default {
 createReservationService,
 getAllReservations,
};
