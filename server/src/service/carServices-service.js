import carServicesData from '../data/carServices-data.js';
import queryUtil from '../common/validation-query-result.js';

const {isQueryEmpty, isThereQueryError, getQueryResponse, doesExist} = queryUtil;


const getAll = async () => {
    const result = await carServicesData.getAllServices();

    return result;
};

const createService = async (req) => {
    const result = await carServicesData.createNewService(req);
    return getQueryResponse(result, [isThereQueryError]);  
};

const update = async (req) => {
    const result = await carServicesData.updateService(req);
    return getQueryResponse(result, [isThereQueryError]);  
};

const deleteService = async (id) => {
    const result = await carServicesData.deleteService(id);
    return getQueryResponse(result, [isThereQueryError]);
};

const getByType = async (type) => {
    const result = await carServicesData.getAllServicesByType(type);
    return getQueryResponse(result, [isThereQueryError]);
};

const getByPrice = async (labor_price) => {
    const result = await carServicesData.getAllServicesByPrice(labor_price);
    return getQueryResponse(result, [isThereQueryError]);
}; 

const getByDate = async (date) => {
    const result = await carServicesData.getAllServicesByDate(date);
    return getQueryResponse(result, [isThereQueryError]);
};
export default {
    getAll,
    createService,
    update,
    deleteService,
    getByType,
    getByPrice,
    getByDate,
};