import brandsData from '../data/brands-data.js';
import cars from '../data/cars-data.js';
import queryUtil from '../common/validation-query-result.js';
import getAllBrandsData from '../data/brands-data.js';

// const {isQueryEmpty, isThereQueryError, getQueryResponse} = queryUtil;

const getAllBrands = async (req) => {
    const result = await getAllBrandsData(req);

    return result;
};
export default getAllBrands;