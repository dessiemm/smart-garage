import queryUtil from '../common/validation-query-result.js';
import ordersData from '../data/orders-data.js';

const {isQueryEmpty, isThereQueryError, getQueryResponse} = queryUtil;

const getAll = async () => {
    const result = await ordersData.getAllOrders();

    return result;
};

const updateStatus = async (req) => {
    const result = await ordersData.updateOrderByStatus(req);
    return getQueryResponse(result, [isThereQueryError]);
};

const getByCustomerId = async (id) => {
    const result = await ordersData.getOrderByCustomerId(id);
    return getQueryResponse(result, [isThereQueryError]);
};

const filterServicesByCarId = async (car_id) => {
    const result = await ordersData.getAllServicesByCarId(car_id);
    return getQueryResponse(result, [isThereQueryError]);
};
export default {
    getAll,
    getByCustomerId,
    filterServicesByCarId,
    updateStatus,
};