import carsData from '../data/cars-data.js';
import cars from '../data/cars-data.js';
import queryUtil from '../common/validation-query-result.js';

const {isQueryEmpty, isThereQueryError, getQueryResponse} = queryUtil;

const getAll = async (req) => {
    const result = await cars.getAllCars(req);

    return result;
};
const getAllBrands = async (req) => {
    const result = await cars.getAllBrands(req);

    return result;
};
const getAllModelsByBrandId = async (id) => {
    const result = await cars.getAllModelsByBrandId(id);
    return result;
};

const getById = async (id) => {
    const result = await cars.getCarById(id);
    return getQueryResponse(result, [isQueryEmpty, isThereQueryError]);
};

const create = async (req) => {
    const result = await carsData.createNewCar(req);
    return getQueryResponse(result, [isThereQueryError]);  
};

const getAllCarTypes = async (req) => {
    const result = await carsData.getAllModelsAndBrands(req);
    
    return result;
};

const update = async (req) => {
    const result = await carsData.updateCar(req);
    return getQueryResponse(result, [isQueryEmpty, isThereQueryError]);
};

const getByCustomerId = async (customer_id) => {
    const result = await carsData.getAllCarsByCustomerId(customer_id);
    return getQueryResponse(result, [isThereQueryError]);
};

const deleteById = async (id) => {
    const result = await carsData.deleteCar(id);
    return getQueryResponse(result, [isThereQueryError]);
};

const getReport = async (customer_id) => {
    const result = await carsData.getFullReportForCustomer(customer_id);
    return getQueryResponse(result, [isThereQueryError]);
  };
export default {
    getAll,
    getById,
    create,
    getAllCarTypes,
    update,
    getByCustomerId,
    deleteById,
    getAllModelsByBrandId,
    getAllBrands,
    getReport,
};
