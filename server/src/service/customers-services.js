import createToken from '../auth/create-token.js';
import bcrypt from 'bcrypt';
import customersData from '../data/customers-data.js';
import queryUtil from '../common/validation-query-result.js';

const { isQueryEmpty, isThereQueryError, getQueryResponse, doesExist } = queryUtil;

const getAll = async (req) => {
  const result = await customersData.getAllCustomers(req);

  return result;
};

const getByName = async (name) => {
  const result = await customersData.getCustomerByName(name);
  return getQueryResponse(result, [isThereQueryError]);
};

const sortByName = async () => {
  const result = await customersData.sortCustomersByName();
  return getQueryResponse(result, [isThereQueryError]);
};

const getByPhone = async (phone) => {
  const result = await customersData.getCustomerByPhone(phone);
  return getQueryResponse(result, [isThereQueryError]);
};

const getByEmail = async (email) => {
  const result = await customersData.getCustomerByEmail(email);
  return getQueryResponse(result, [isThereQueryError]);
};

const getByModel = async (model) => {
  const result = await customersData.getCustomerByModel(model);
  return getQueryResponse(result, [isThereQueryError]);
};

const getByDateRange = async (date_from, date_to) => {
  const result = await customersData.getCustomerByDateRange(date_from, date_to);
  return getQueryResponse(result, [isQueryEmpty, isThereQueryError]);
};

const getById = async (id) => {
  const result = await customersData.getCustomerById(id);
  return getQueryResponse(result, [isQueryEmpty, isThereQueryError]);
};

const getReport = async () => {
  const result = await customersData.getFullReportForCustomer();
  return getQueryResponse(result, [isQueryEmpty, isThereQueryError]);
};

const getTable = async () => {
  const result = await customersData.getFullTable();
  return result;
};

const createNewCustomer = async (customer) => {
  const customerExists = (await customersData.getCustomerByEmail(customer.email))[0];
  if (customerExists) {
    return {
      error: true,
      response: {
        error: 'Such customer already exists!',
      },
    };
  }

  const newCustomer = await customersData.createCustomer(customer);
  if (!newCustomer) {
    return {
      error: true,
      response: {
        error: 'Please enter valid details to make an account!',
      },
    };
  }
  return {
    error: null,
    response: newCustomer,
  };

};

const newUpdatedPassword = async (customer) => {
  const result = await customersData.sendNewPassword(customer);
  return getQueryResponse(result, [isThereQueryError]);
};

const validateUser = async (customer) => {
  const { error, response } = await customersData.getCustomerBy('email', customer.email);

  if (error) {
    return {
      error: true,
      response: {
        error: 'There is no such user!',
      },
    };
  }
  if (await bcrypt.compare(customer.password, response.password)) {
    const {
      id,
      fullname,
      role,
    } = response;
    const token = createToken({ 
      id,
      fullname,
      role,
    });
    return {
      error: null,
      response: {
        token,
      },
    };
  }
  return {
    error: true,
    response: {
      error: 'Invalid password!',
    },
  };
};

const logoutCustomer = async (token) => {
  await customersData.logoutCustomer(token);
  return {
    error: false,
    response: 'You have successfully been logged out!',
  };
};

const updateCustomerInfo = async (req) => {
  const result = await customersData.updateCustomer(req);
  return getQueryResponse(result, [isThereQueryError]);
};

const deleteCustomer = async (id) => {
  const result = await customersData.deleteCustomer(id);
  return getQueryResponse(result, [isThereQueryError]);
};

export default {
  getAll,
  getById,
  getByEmail,
  getByName,
  sortByName,
  getByPhone,
  getByModel,
  getByDateRange,
  getReport,
  getTable,
  logoutCustomer,
  createNewCustomer,
  validateUser,
  newUpdatedPassword,
  updateCustomerInfo,
  deleteCustomer,
};