import express from 'express';
import getAllModels from '../service/models-service.js';

const modelsRouter = express.Router();

modelsRouter
.get('/:id', async (req, res) => {
    const {
        error,
        response,
    } = await getAllModels(+req.params.id);

    if (error) {
        res.status(404).json({ message: response.error });
    } else {
        res.status(200).json(response.response);
    };
});
export default modelsRouter;