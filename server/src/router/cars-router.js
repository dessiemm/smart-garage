import express from 'express';
import carsService from '../service/cars-service.js';
import validateBody from '../middlewares/validate-body.js';
import createCarValidator from '../validators/create-car-validator.js';
// import serviceErrors from '../services/service-errors.js';

const carsRouter = express.Router();

carsRouter
    .get('/', async (req, res) => {
        let response;

        if (req.query.customer_id) {
            response = await carsService.getByCustomerId(req.query.customer_id);
        } else {
            response = await carsService.getAll();
        }
        res.status(200).json(response.response);
    })
    .get('/cartypes', async (req, res) => {
        const response = await carsService.getAllCarTypes(req);

        res.status(200).json(response.response);
    })
    .get('/:id', async (req, res) => {
        const {
            error,
            response,
        } = await carsService.getById(+req.params.id);

        if (error) {
            res.status(404).json({ message: response.error });
        } else {
            res.status(200).json(response.response);
        };
    })
    .get('/report', async (req, res) => {
        let response;

        if (req.query.customer_id) {
            response = await carsService.getReport(req.query.customer_id);
        }

        res.status(200).json(response);
    })
    .get('/models', async (req, res) => {
        try {
            const { error, response } = await carsService.getAllModelsAndBrands(req.query);
            if (error) {
                return res.status(error).json({ error: response });
            }
            res.status(200).json(response);
        }
        catch (error) {
            res.status(400).json(error);
        };
    })
    .get('/brands', async (req, res) => {
        try {
            const { error, response } = await carsService.getAllBrands(req.query);
            if (error) {
                return res.status(error).json({ error: response });
            }
            res.status(200).json(response);
        }
        catch (error) {
            res.status(400).json(error);
        };
    })
    .post('/', validateBody('createCar', createCarValidator), async (req, res) => {
        const {
            error,
            response,
        } = await carsService.create(req);

        if (error) {
            res.status(404).json({ message: response.error });
        } else {
            res.status(200).json(response.response);
        }
    })
    .put('/:car_id', validateBody('createCar', createCarValidator), async (req, res) => {
        const {
            error,
            response,
        } = await carsService.update(req);

        if (error) {
            res.status(400).json({ message: response.error });
        } else {
            res.status(200).json(response.response);
        };
    })
    .delete('/:id', async (req, res) => {
        const response = await carsService.deleteById(+req.params.id);

        if (response.error) {
            res.status(404).json('There is an error deleting that car!');
        } else {
            res.status(200).json('Car deleted successfully!');;
        };
    });


export default carsRouter;