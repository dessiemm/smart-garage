import express from 'express';
import carServicesService from '../service/carServices-service.js';

const carServicesRouter = express.Router();

carServicesRouter
    .get('/', async (req, res) => {
        let response;

        if (req.query.type) {
            response = await carServicesService.getByType(req.query.type);
        } else if (req.query.labor_price) {
            response = await carServicesService.getByPrice(req.query.labor_price);
        } else if (req.query.date) {
            response = await carServicesService.getByDate(req.query.date);
        } else {
            response = await carServicesService.getAll();
        };
        res.status(200).json(response.response);
    })
    .get('/models', async (req, res) => {
        let response;

        if (req.query.type) {
            response = await carServicesService.getAllModelsByBrandId(req.query.type);
        } 
        
        res.status(200).json(response.response);
    })
    .post('/', async (req, res) => {
        const {
            error,
            response,
        } = await carServicesService.createService(req);

        if (error) {
            res.status(404).json({ message: response.error });
        } else {
            res.status(200).json(response.response);
        };
    })
    .put('/:id', async (req, res) => {
        const {
            error,
            response,
        } = await carServicesService.update(req);

        if (error) {
            res.status(404).json({ message: response.error });
        } else {
            res.status(200).json(response.response);
        };
    })
    .delete('/:id', async (req, res) => {
        const response = await carServicesService.deleteService(req);

        if (response.error) {
            res.status(404).json('This service cannot be deleted!');
        } else {
            res.status(200).json('Service deleted successfully!');
        };
    });

export default carServicesRouter;