import express from 'express';
import bcrypt from 'bcrypt';
import { sendEmailTemplate } from '../common/email-send.js';
import validateBody from '../middlewares/validate-body.js';
// import validateBody from '../middleware/validate-body.js';
// import createUserValidator from '../validators/create-user-validator.js';
// import userServices from '../services/user-services.js';
// import bcrypt from 'bcrypt';
// import updateUserValidator from '../validators/update-user-validator.js';
import serviceErrors from '../service/service-errors.js';
// import customersData from '../data/customers-data.js';
import customerServices from '../service/customers-services.js';
import createCustomerValidator from '../validators/create-customer-validator.js';
import loginValidator from '../validators/login-validator.js';
import forgottenPasswordValidator from '../validators/forgotten-password-validator.js';

const customerRouter = express.Router();

customerRouter
    .get('/', async (req, res) => {
        let response;

        if (req.query.name) {
            response = await customerServices.getByName(req.query.name);
        } else if (req.query.phone) {
            response = await customerServices.getByPhone(req.query.phone);
        } else if (req.query.email) {
            response = await customerServices.getByEmail(req.query.email);
        } else if (req.query.model) {
            response = await customerServices.getByModel(req.query.model);
        } else if (req.query.date_from && req.query.date_to) {
            response = await customerServices.getByDateRange(req.query.date_from, req.query.date_to);
        } else {
            response = await customerServices.getAll();
        };
        if (response.error) {
            res.status(404).json(response.response.error);
        } else {
            res.status(200).json(response.response);
        }
    })
    .get('/report', async (req, res) => {
        const response = await customerServices.getReport();
        res.status(200).json(response.response.response);
    })
    .get('/table', async (req, res) => {
        const response = await customerServices.getTable(req);

        if (response.error) {
            res.status(404).json(response.response.error);
        } else {
            res.status(200).json(response.response);
        };
    })
    .get('/:id', async (req, res) => {
        const {
            error, 
            response,
        } = await customerServices.getById(+req.params.id);

        if (error) {
            res.status(404).json({ message: response.error });
        } else {
            res.status(200).json(response);
        };
    })
    .post('/register', validateBody('register', createCustomerValidator), async (req, res) => {
        const customer = req.body;
        const randomPassword = 'customer' + Math.round(Math.random() * 1000);
        customer.password = await bcrypt.hash(randomPassword, 10);
        const {
            error,
            response,
        } = await customerServices.createNewCustomer(customer);

        if (error) {
            res.status(400).json({
                message: response.error,
            });
        } else {
            res.status(200).json(response);
            const emailInfo = {
                name: customer.firstname + ' ' + customer.lastname,
                email: customer.email,
                password: randomPassword,
            };
            sendEmailTemplate(emailInfo, 'newcustomer');
        }
    })

    .post('/login', validateBody('login', loginValidator), async (req, res) => {
        const {
            error,
            response,
        } = await customerServices.validateUser(req.body);

        if (error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).json({
                message: 'There is not such user!',
            });
        } else {
            res.status(200).json(response);
        }
    })
  
    .put('/forgotten', validateBody('forgotten', forgottenPasswordValidator), async (req, res) => {
        try {
            const customer = req.body;
            const newPassword = 'customer' + Math.round(Math.random() * 1000);
            customer.password = await bcrypt.hash(newPassword, 10);

            const {
                error,
                response,
            } = await customerServices.newUpdatedPassword(customer);

            if (error) {
                res.status(error).json({ error: response });
            } else {
                res.status(200).json(response.response);

                const emailInfo = {
                    email: customer.email,
                    password: newPassword,
                };
                sendEmailTemplate(emailInfo, 'newcustomer');
            }
        } catch (error) {
            res.status(400).json(error);
        }
    })
    .put('/:id', async (req, res) => {
        const {
            error,
            response,
        } = await customerServices.updateCustomerInfo(req);

        if (error) {
            res.status(404).json({ message: response.error });
        } else {
            res.status(200).json(response.response);
        };
    })
    //logout
    .delete('/logout', async (req, res) => {
        const {
            error,
            response,
        } = await customerServices.logoutCustomer(req.headers.authorization.replace('Bearer ', ''));
        if (error === service.RECORD_NOT_FOUND) {
            res.status(404).json({
                message: 'There is not such user!',
            });
        } else {
            res.status(401).json({
                message: 'The user has successfully logged out!',
            });
        }
    })
    .delete('/:id', async (req, res) => {
        const response = await customerServices.deleteCustomer(+req.params.id);

        if (response.error) {
            res.status(404).json('There is an error deleting that customer!');
        } else {
            res.status(200).json('Customer deleted successfully!');;
        };
    });

export default customerRouter;