import express from 'express';
import validateBody from '../middlewares/validate-body.js';
import ordersService from '../service/orders-service.js';
import updateStatusValidator from '../validators/update-status-validator.js';

const ordersRouter = express.Router();

ordersRouter
    .get('/', async (req, res) => {
        let response;

        if (req.query.car_id) {
            response = await ordersService.filterServicesByCarId(req.query.car_id);
        } else {
            response = await ordersService.getAll();
        }
        res.status(200).json(response.response);
    })
    .get('/:id', async (req, res) => {
        const {
            error,
            response,
        } = await ordersService.getByCustomerId(+req.params.id);
        if (error) {
            res.status(400).json({ message: response.error });
        } else {
            res.status(200).json(response.response);
        }
    })
    .put('/:id', async (req, res) => {
        const {
            error,
            response,
        } = await ordersService.updateStatus(req);

        if (error) {
            res.status(404).json({ message: response.error });
        } else {
            res.status(200).json(response.response);
        };
    });

export default ordersRouter;