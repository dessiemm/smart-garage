import express from 'express';
import getAllBrands from '../service/brands-service.js';
// import validateBody from '../middlewares/validate-body.js';
//import createCarValidator from '../validators/create-car-validator.js';
// import serviceErrors from '../services/service-errors.js';

const brandsRouter = express.Router();

brandsRouter
.get('/', async (req, res) => {
    const response = await getAllBrands(req);
    
    res.status(200).json(response.response);
});
// .get('/models', async (req, res) => {
//     const response = await carsService.getAllModelsByBrandId(req);
//     console.log(response)
//     res.status(200).json(response.response);
// })
export default brandsRouter;