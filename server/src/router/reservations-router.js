import express from 'express';
import reservationsService from '../service/reservations-service.js';

const reservationsRouter = express.Router();

reservationsRouter
.get('/', async (req, res) => {
    let response = await reservationsService.getAllReservations(); 

    res.status(200).json(response.response);
})
.post('/', async (req, res) => {
    const {
        error,
        response,
    } = await reservationsService.createReservationService(req);
    console.log(response, 'reservations-router');
    if (error) {
        res.status(404).json({ message: response.error });
    } else {
        res.status(200).json(response);
    };
})
;
export default reservationsRouter;