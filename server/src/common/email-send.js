import pool from '../data/pool.js';
import mailjet from 'node-mailjet';

export const sendEmail = (emailTo, type, subject, HTMLEmailbody) => {
    const jet = mailjet
        .connect('f7a4ef84ad33aad2ac6ca2d42e6bcc6d', '6804d1441403906ff70ef12247489715');
    const request = jet
        .post('send', { 'version': 'v3.1' })
        .request({
            'Messages': [
                {
                    'From': {
                        'Email': 'desislava_b_milanova@abv.bg',
                        'Name': 'Smart Garage',
                    },
                    'To': [
                        {
                            'Email': emailTo,
                            'Name': type,
                        },
                    ],
                    'Subject': subject,
                    'HTMLPart': HTMLEmailbody,
                    'CustomID': 'AppGettingStartedTest',
                },
            ],
        });
    request
        .then((result) => {
            console.log(result.body);
        })
        .catch((err) => {
            console.log(err.statusCode);
        });
};

export const sendEmailTemplate = async (emailInfo, template) => {
    const sql = `
    SELECT
      subject,
      HTML
    FROM html_templates
    WHERE type = ?
    `;

    const HTMLtemplate = (await pool.query(sql, [template]))[0];
    const HTMLEmailbody = Object.keys(emailInfo)
    // .map((element,_ , arr) => {
    //     return arr.replace(`##${element}##`, emailInfo[element]);
    // });
        .reduce((acc, e) => {
            return acc.replace(`##${e}##`, emailInfo[e]);
        }, HTMLtemplate.HTML);

    sendEmail(emailInfo.email, emailInfo.type, HTMLtemplate.subject, HTMLEmailbody);
};