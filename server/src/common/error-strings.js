export default {
    register: {
          firstname: 'Please enter a valid firstname',
          lastname: 'Please enter a valid lastname',
          email: 'Please enter a valid email',
          phone: 'Please enter a valid phone number',
    },

    login: {
        email: 'Please enter a valid email',
        password: 'Please enter a valid password',
    },

    forgotten: {
          email: 'Please enter a valid email',
      //     password: 'Please enter a valid password',
            
    },

    createCar: {
          vin: 'Please enter a valid VIN with length of 17',
          registration_plate: 'Please enter a valid BG registration plate with length of 8',
          year: 'Please enter a valid year',
    },

    updateStatus: {
          status: 'The status should be one of the following : pending, ongoing, done',
    },

};