const isThereQueryError = (result, message) => {
    if (result.error) {
        return message || 'There is an error with the query!';
        };
};

const isQueryEmpty = (result, message) => {
    if (result.response.length === 0) {
        return message || 'The query you are searching by is empty!';
    };
};

const doesExist = (result, message) => {
    if (result.response.length !== 0) {
        return message || 'This record already exists';
    };
};

const getQueryResponse = (result, validations, message) => {
    
    const validation = validations.find(v => v(result));
    if (validation) {
        return {
            error: true,
            response: {
                error: validation(result, message),
            },
        };
    } else {
        return {
            error: false,
            response: result,
        };
    }
};

export default {
    isQueryEmpty,
    isThereQueryError,
    doesExist,
    getQueryResponse,
};