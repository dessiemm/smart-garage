import pool from './pool.js';

export const tokenExists = async (token) => {
  const result = await pool.query('SELECT * FROM tokens t WHERE t.token = ? ', [token]);

  return result && result.length > 0;
};