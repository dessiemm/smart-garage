import mariadb from 'mariadb';
import dotenv from 'dotenv';
export const DOT_ENV_CONFIG = dotenv.config().parsed;

export default mariadb.createPool({
  host: DOT_ENV_CONFIG.DB_HOST,
  port: DOT_ENV_CONFIG.DB_PORT,
  user: DOT_ENV_CONFIG.DB_USER,
  password: DOT_ENV_CONFIG.DB_PASSWORD,
  database: DOT_ENV_CONFIG.DB_NAME,
  connectionLimit: 1,
});