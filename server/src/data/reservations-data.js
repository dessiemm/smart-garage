import pool from './pool.js';

const getAllReservations = async () => {
  let result;
  const sql = `
  SELECT
    r.id,
    r.email,
    m.name as model,
    brands.name as brand,
    services.type as service_type,
    r.vin,
    r.year,
    r.name,
    r.phone
  FROM reservations as r
  INNER JOIN models as m on
    r.model_id = m.id
  INNER JOIN brands on 
    r.brand_id = brands.id
  INNER JOIN services on
    r.service_id = services.id
  `;
  
  result = await pool.query(sql);

  return {
    error: false,
    response: result,
  };

};

const createReservation = async (req) => {
    const sql = `
    INSERT INTO reservations SET
      brand_id = ?,
      model_id = ?,
      service_id = ?,
      vin = ?,
      year = ?,
      email = ?,
      name = ?,
      phone = ?
    `;
  
    const result = await pool.query(sql, [req.body.brand_id, req.body.model_id, req.body.service_id, req.body.vin, req.body.year, req.body.name, req.body.email, req.body.phone]);
    
    return {
      error: false,
      response: result,
    };

  };
  export default {
    createReservation, 
    getAllReservations,
 };