import pool from './pool.js';

const getAllOrders = async () => {
    let result;

    const sql = `
    SELECT 
        concat_ws(' ', customers.firstname, customers.lastname) as fullname,
        services.type,
        services.labor_cost,
        orders.order_date,
        orders.status,
        orders.id
    FROM orders
    INNER JOIN services on
        services.id = orders.service_id
    INNER JOIN cars on
        cars.id = orders.car_id
    INNER JOIN customers on 
    customers.id = cars.customer_id

    `;
 
    result = await pool.query(sql);

    return {
        error: false,
        response: result,
    };
};

const updateOrderByStatus = async (req) => {
    let result;

    const sql = `
        UPDATE orders
        SET status = ?
        WHERE id = ?
    `;

    result = await pool.query(sql, [req.body.status, req.params.id]);

    return {
        error: false,
        response: 'Order status is updated',
    };
};

const getOrderByCustomerId = async (id) => {
    let result;

    const sql = `
    SELECT 
        concat_ws(' ', cs.firstname, cs.lastname) as fullname,
        o.order_date,
        o.status,
        s.type,
        s.labor_cost,
        c.vin,
        c.registration_plate
    FROM orders as o
    INNER JOIN services as s on
        s.id = o.service_id
    INNER JOIN cars as c on
        c.id = o.car_id
    INNER JOIN customers as cs on
        cs.id = c.customer_id
    WHERE customer_id = ?
    `;

    result = await pool.query(sql, id);

    return {
        error: false,
        response: result,
    };
};

const getAllServicesByCarId = async (car_id) => {
    let result;
    const sql = `
SELECT 
    o.id as order_id, 
    s.type as service_type, 
    s.labor_cost as price
FROM orders as o
INNER JOIN services as s ON
    s.id = o.service_id
WHERE car_id = ?
`;
    result = await pool.query(sql, car_id);

    return {
        error: false,
        response: result,
    };
};

export default {
    getAllOrders,
    getOrderByCustomerId,
    getAllServicesByCarId,
    updateOrderByStatus,
};