import { response } from 'express';
import pool from './pool.js';

const getAllServices = async () => {
    let result;
    const sql = `
    SELECT 
        services.id,
        services.type,
        services.labor_cost,
        services.description
    FROM services
    `;

    result = await pool.query(sql);
    console.log('services');
    return {
        error: false,
        response: result,
    };
};

const createNewService = async (req) => {
    let result;
    const sql = `
    INSERT INTO services (type, labor_cost)
    VALUES (?, ?)
    `;

    result = await pool.query(sql, [req.body.type, req.body.labor_cost]);

    return {
        error: false,
        response: 'New service has been added!',
    };
};

const updateService = async (req) => {
    let result;
    const sql = `
    UPDATE services
    SET type = ?, labor_cost = ?
    WHERE id = ?  
    `;

    result = await pool.query(sql, [req.body.type, req.body.labor_cost, req.params.id]);

    return {
        error: false,
        response: 'Service updated!',
    };
};

const deleteService = async (req) => {
    let result;
    const sql = `
    DELETE FROM services
    WHERE id = ?
    `;

    result = await pool.query(sql, [req.params.id]);

    if (result.affectedRows === 0) {
        return {
            error: true,
            response: 'Cannot delete service that does not exist!',
        };
    } else {
        return {
            error: false,
            response: result,
        };
    };
};

const getAllServicesByType = async (type) => {
    let result;
    const sql = `
    SELECT 
        services.type,
        services.labor_cost,
        orders.order_date
    FROM services
    INNER JOIN orders on
        services.id = orders.service_id
    WHERE type = ?
    `;

    result = await pool.query(sql, type);

    return {
        error: false,
        response: result,
    };
};

const getAllServicesByPrice = async (labor_cost) => {
    let result;
    const sql = `
    SELECT 
        services.type,
        services.labor_cost,
        orders.order_date
    FROM services
    INNER JOIN orders on
        services.id = orders.service_id
    WHEN labor_cost = ?
    `;

    result = await pool.query(sql, labor_cost);

    return {
        error: false,
        response: result,
    };
};

const getAllServicesByDate = async (date) => {
    let result;
    const sql = `
    SELECT 
        services.type,
        services.labor_cost,
        orders.order_date
    FROM services
    INNER JOIN orders on
        services.id = orders.service_id
    WHERE 
        order_date BETWEEN ? and ?; 
    `;

    result = await pool.query(sql, date);

    return {
        error: false,
        response: result,
    };
};
export default {
    getAllServices,
    createNewService,
    updateService,
    deleteService,
    getAllServicesByType,
    getAllServicesByPrice,
    getAllServicesByDate,
};