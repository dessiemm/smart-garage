import pool from './pool.js';
import bcrypt from 'bcrypt';

const getAllCustomers = async () => {
  let result;
  const sql = `
  SELECT 
    c.id,
    c.firstname,
    c.lastname,
    c.email,
    c.phone
  FROM customers as c
  
  `;

  result = await pool.query(sql);
  console.log('all');
  return {
    error: false,
    response: result,
  };

};

const getCustomerByName = async (name) => {
  let result;
  let sql = `
  SELECT 
  concat_ws(' ', firstname, lastname) as fullname,
  email,
  phone
  FROM customers
  WHERE firstname = ?
  `;

  result = await pool.query(sql, name);

  return {
    error: false,
    response: result,
  };
};

const sortCustomersByName = async () => {
  let result;
  const sql = `
  SELECT 
    concat_ws(' ', firstname, lastname) as fullname
  FROM customers
  ORDER BY firstname ASC;
  `;

  result = await pool.query(sql);

  return {
    error: false,
    response: result,
  };
};

const getCustomerByEmail = async (email) => {
  let result;
  let sql = `
  SELECT 
  concat_ws(' ', firstname, lastname) as fullname,
  email,
  phone
  FROM customers
  WHERE email = ?
  `;

  result = await pool.query(sql, email);

  return {
    error: false,
    response: result,
  };
};
const getCustomerByPhone = async (phone) => {
  let result;
  let sql = `
  SELECT 
  concat_ws(' ', firstname, lastname) as fullname,
  email,
  phone
  FROM customers
  WHERE phone = ?
  `;

  result = await pool.query(sql, phone);

  return {
    error: false,
    response: result,
  };
};

const getCustomerByModel = async (name) => {
  let result;
  let sql = `
  SELECT 
    concat_ws(' ', c.firstname, c.lastname) as fullname,
    c.email,
    c.phone,
    models.name as model
  FROM customers as c
  INNER JOIN cars on 
    c.id = cars.customer_id
  INNER JOIN models on 
    models.id = cars.model_id
  WHERE models.name = ?;
  `;

  result = await pool.query(sql, name);

  return {
    error: false,
    response: result,
  };
};

const getCustomerByDateRange = async (date_from, date_to) => {
  let result;
  const sql = `
  SELECT 
    concat_ws(' ', customers.firstname, customers.lastname) as fullname,
    customers.email,
    customers.phone
  FROM customers
  INNER JOIN cars on
    customers.id = cars.customer_id
  INNER JOIN orders on
    cars.id = orders.car_id
  WHERE 
    order_date BETWEEN ? and ?; 
`;

  result = await pool.query(sql, [date_from, date_to ]);

  return {
    error: false,
    response: result,
  };
};

const getCustomerById = async (id) => {
  let result;
  const sql = `
  SELECT
    *
  FROM
    customers
  WHERE
    id = ?
  `;

  result = await pool.query(sql, id);

  return {
    error: false,
    response: result,
  };
};

const createCustomer = async (customer) => {
  const res = await pool.query('SELECT * FROM customers c WHERE c.email = ?', [customer.email]);
  if (res[0]) {
    return {
      error: true,
      response: {
        error: 'Email should be unique!',
      },
    };
  }

  const sql = `
    INSERT INTO customers (firstname, lastname, email, password, phone, role)
    VALUES (?, ?, ?, ?, ?, 'default')
  `;

  const result = await pool.query(sql, [customer.firstname, customer.lastname, customer.email, customer.password, customer.phone]);

  const sql2 = `
    SELECT c.id, c.firstname, c.lastname, c.email, c.phone, c.password  
    FROM customers c
    WHERE c.id = ?
  `;

  const createdUser = (await pool.query(sql2, [result.insertId]))[0];

  return createdUser;

};

const getCustomerBy = async (column, value) => {
  const sql = `
  SELECT
  c.id,
  concat_ws(' ', c.firstname, c.lastname) as fullname,
  c.email,
  c.password,
  c.phone,
  c.role
  FROM customers as c WHERE ${column} = ?
  `;
  const existingCustomer = (await pool.query(sql, [value]))[0];
  if (!existingCustomer) {
    return {
      error: true,
      response: 'There is no such user',
    };
  }
  return {
    error: false,
    response: existingCustomer,
  };
};

const getFullReportForCustomer = async () => {
  let result;
  const sql = `
  SELECT
    services.id,
	  concat_ws(' ', customers.firstname, customers.lastname) as fullname,
	  customers.email,
	  customers.phone,
    b.name as brand,
	  models.name as model,
	  c.vin,
	  c.registration_plate,
	  c.year,
    services.type as service_type,
	  services.labor_cost
  FROM cars as c 
  INNER JOIN customers on
	  customers.id = c.customer_id
  INNER JOIN orders on
	  orders.car_id = c.id
  INNER JOIN services on
	  services.id = orders.service_id
  INNER JOIN models on
	  models.id = c.model_id
  INNER JOIN brands as b on
	  b.id = models.brand_id
  `;

  result = await pool.query(sql);

  return {
    error: false,
    response: result,
  };
};

const getFullTable = async () => {
  let result;
  const sql = `
  SELECT
	  concat_ws(' ', customers.firstname, customers.lastname) as fullname,
	  customers.email,
	  customers.phone,
    b.name as car_brand,
	  models.name as car_model,
	  c.vin,
	  c.registration_plate,
	  c.year,
    services.type as service_type,
	  services.labor_cost
  FROM cars as c 
  INNER JOIN customers on
	  customers.id = c.customer_id
  INNER JOIN orders on
	  orders.car_id = c.id
  INNER JOIN services on
	  services.id = orders.service_id
  INNER JOIN models on
	  models.id = c.model_id
  INNER JOIN brands as b on
	  b.id = models.brand_id
  `;

  result = await pool.query(sql);

  return {
    error: false,
    response: result,
  };
};
const sendNewPassword = async (customer) => {
  const existingEmail = (await pool.query('SELECT * FROM customers c WHERE c.email = ?', [customer.email]))[0];

  if (!existingEmail) {
    return {
      error: true,
      response: {
        error: 'No user exists with such Email!',
      },
    };
  }
  const sql = `
    UPDATE customers SET password = ? 
    WHERE email = ?
  `;

  await pool.query(sql, [customer.password, customer.email]);

  return {
    error: null,
    response: 'Your password has been updated',
  };
};

const logoutCustomer = async (token) => {
  return await pool.query('INSERT INTO tokens (token) VALUES (?)', [token]);
};

const updateCustomer = async (req) => {
  let result;
  const sql = `
  UPDATE customers
  SET firstname = ?, lastname = ?, email=  ?, phone = ?
  WHERE id = ?
  `;

  result = await pool.query(sql, [req.body.firstname, req.body.lastname, req.body.email, req.body.phone, req.params.id]);

  return {
    error: false,
    response: 'Customer info is updated',
  };
};

const deleteCustomer = async (id) => {
  let result;
  const sql = `
  DELETE from customers
  WHERE id = ?
  `;

  result = await pool.query(sql, [id]);

  if (result.affectedRows === 0) {
    return {
      error: true,
      response: 'Cannot delete customer that does not exist!',
    };
  } else {
    return { 
      error: false,
      response: result,
  };
};  
};

export default {
  getAllCustomers,
  getCustomerById,
  createCustomer,
  getFullReportForCustomer,
  getFullTable,
  getCustomerByPhone,
  getCustomerByName,
  sortCustomersByName,
  getCustomerByEmail,
  getCustomerByModel,
  getCustomerByDateRange,
  sendNewPassword,
  getCustomerBy,
  getCustomerByEmail,
  logoutCustomer,
  updateCustomer,
  deleteCustomer,
};