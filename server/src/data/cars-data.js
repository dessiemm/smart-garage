import { response } from 'express';
import pool from './pool.js';

const getAllCars = async () => {
    let result;
    const sql = `
    SELECT 
        cars.id,
        b.name as brand,
        m.name as model, 
        cars.vin,
        cars.registration_plate,
        cars.year,
		concat_ws(' ', customers.firstname, customers.lastname) as fullname
    FROM cars
    INNER JOIN models as m on
        m.id = cars.model_id
    INNER JOIN brands as b on
        b.id = m.brand_id
	INNER JOIN customers on 
		customers.id = cars.customer_id
    `;

    result = await pool.query(sql);

    return {
        error: false,
        response: result,
    };

};
const getAllBrands = async () => {
    let result;
    const sql = `
    SELECT
    *
    FROM
        brands
    `;

    result = await pool.query(sql);

    return {
        error: false,
        response: result,
    };
};

const getCarById = async (id) => {
    let result;
    const sql = `
    SELECT
      *
    FROM
      cars
    WHERE 
      id = ?
    `;

    result = await pool.query(sql, id);

    return {
        error: false,
        response: result,
    };
};

const createNewCar = async (req) => {
    let result;

    const sql = `
    INSERT INTO cars (model_id, vin, registration_plate, customer_id, year)
    VALUES (?, ?, ?, ?, ?)
    `;

    result = await pool.query(sql, [req.body.model_id, req.body.vin, req.body.registration_plate, req.body.customer_id, req.body.year]);

    return {
        error: false,
        response: 'New car has been added!',
    };
};

const getAllModelsAndBrands = async () => {
    let result;

    const sql = `
    SELECT
        m.id, m.name as model, b.name as brand
    FROM
        models as m
    INNER JOIN brands as b on
        b.id = m.brand_id
    `;

    result = await pool.query(sql);

    return {
        error: false,
        response: result,
    };
};
const getModelById = async (id) => {
    let result;
    const sql = `
    SELECT
      *
    FROM
      models
    WHERE 
      id = ?
    `;

    result = await pool.query(sql, id);

    return {
        error: false,
        response: result,
    };
};
// const getAllModelsByBrandId = async (id) => {
//     let result;
//     const sql = `
//     SELECT  
//          m.name
//     FROM brands AS b
//     INNER JOIN  models AS m ON 
//         m.brand_id = b.id
//     WHERE b.id = 2
//     `;

//     result = await pool.query(sql, id);

//     return {
//         error: false,
//         response: result,
//     };
// };

const updateCar = async (req) => {
    let result;

    const sql = `
    UPDATE cars 
    SET vin = ?, registration_plate = ?, year = ?
    WHERE id = ?
    `;

    result = await pool.query(sql, [req.body.vin, req.body.registration_plate, req.body.year, req.params.car_id]);

    return {
        error: false,
        response: 'Car updated!',
    };
};

const getAllCarsByCustomerId = async (customer_id) => {
    let result;
    const sql = `
    SELECT 
        b.name as brand,
        m.name as model, 
        cars.vin,
        cars.registration_plate,
        cars.year
    FROM cars
    INNER JOIN models as m on
	    m.id = cars.model_id
    INNER JOIN brands as b on
	    b.id = m.brand_id
    WHERE customer_id = ?
    `;
    result = await pool.query(sql, customer_id);

    return {
        error: false,
        response: result,
    };
};

const deleteCar = async (id) => {
    let result;
    const sql = `
    DELETE FROM cars
    WHERE id = ?
    `;

    result = await pool.query(sql, [id]);

    if (result.affectedRows === 0) {
        return {
            error: true,
            response: 'Cannot delete car that does not exist!',
        };
    } else {
        return {
            error: false,
            response: result,
        };
    };
};

const getFullReportForCustomer = async (customer_id) => {
    let result;
    const sql = `
    SELECT
      cars.customer_id,
        concat_ws(' ', customers.firstname, customers.lastname) as fullname,
        customers.email,
        customers.phone,
      b.name as brand,
        models.name as model,
        c.vin,
        c.registration_plate,
        c.year,
      services.type as service_type,
        services.labor_cost
    FROM cars as c 
    INNER JOIN customers on
        customers.id = c.customer_id
    INNER JOIN orders on
        orders.car_id = c.id
    INNER JOIN services on
        services.id = orders.service_id
    INNER JOIN models on
        models.id = c.model_id
    INNER JOIN brands as b on
        b.id = models.brand_id
    WHERE c.customer_id = ?
    `;
  
    result = await pool.query(sql, customer_id);
  
    return {
      error: false,
      response: result,
    };
  };
export default {
    getAllCars,
    getCarById,
    createNewCar,
    getAllModelsAndBrands,
    updateCar,
    getAllCarsByCustomerId,
    deleteCar,
    // getAllModelsByBrandId,
    getModelById,
    getAllBrands,
    getFullReportForCustomer,
};