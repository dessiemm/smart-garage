import { response } from 'express';
import pool from './pool.js';

const getAllBrandsData = async () => {
    let result;
    const sql = `
    SELECT
        brands.name as brand
    FROM
        brands
    `;

    result = await pool.query(sql);

    return {
        error: false,
        response: result,
    };
};

export default getAllBrandsData;