import pool from './pool.js';

const getAllModelsByBrandId = async (id) => {
    let result;
    const sql = `
    SELECT  
         m.name as model
    FROM brands AS b
    INNER JOIN  models AS m ON 
        m.brand_id = b.id
    WHERE b.id = ?
    `;

    result = await pool.query(sql, id);

    return {
        error: false,
        response: result,
    };
};
export default getAllModelsByBrandId;