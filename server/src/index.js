import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import customerRouter from './router/customers-router.js';
import carsService from './service/cars-service.js';
import carsRouter from './router/cars-router.js';
import carServicesService from './service/carServices-service.js';
import carServicesRouter from './router/carServices-router.js';
import ordersRouter from './router/orders-router.js';
import loggedCustomerGuard from './middlewares/logged-customer-guard.js';
import brandsRouter from './router/brands-router.js';
import modelsRouter from './router/models-router.js';
import reservationsRouter from './router/reservations-router.js';

const PORT = 5555;

const app = express();

app.use(express.json());
app.use(cors());
app.use(helmet());
passport.use(jwtStrategy);
app.use(passport.initialize());


// app.use('/books', authMiddleware, loggedUserGuard, reviewsRouter);
app.use('/customers', customerRouter);
app.use('/cars', carsRouter);
app.use('/services', carServicesRouter);
app.use('/orders', ordersRouter);
app.use('/brands', brandsRouter);
app.use('/models', modelsRouter);
app.use('/reservations', reservationsRouter);
app.listen(PORT, () => console.log(`App listening on port ${PORT}!`));