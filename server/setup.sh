echo "{
  \"name\": \"not-a-twitter-api\",
  \"version\": \"1.0.0\",
  \"description\": \"\",
  \"main\": \"src/index.js\",
  \"type\": \"module\",
  \"scripts\": {
    \"start\": \"node src/index.js\",
    \"start:dev\": \"nodemon src/index.js\"
  },
  \"keywords\": [],
  \"author\": \"\",
  \"license\": \"ISC\"
}" >> package.json

npm i babel-eslint babel-jest @babel/preset-env regenerator-runtime @babel/plugin-syntax-dynamic-import eslint nodemon helmet express cors

echo "{
  \"presets\": [\"@babel/preset-env\"],
  \"plugins\": [\"@babel/plugin-syntax-dynamic-import\"]
}" >> .babelrc

echo "// eslint-disable-next-line no-undef
module.exports = {
	'env': {
		'es2020': true,
		'node': true,
	},
	'parser': 'babel-eslint',
	'parserOptions': {
		'sourceType': 'module',
		'ecmaVersion': 11,
		'ecmaFeatures': {
			'impliedStrict': true,
		},
	},
	'extends': 'eslint:recommended',
	'rules': {
		'quotes': ['error', 'single'],
		'semi': ['error', 'always'],
		'comma-dangle': ['warn', 'always-multiline'],
		'max-classes-per-file': ['error', 1],
	},
};" >> .eslintrc.js

mkdir src

touch src/index.js