import './App.css';
import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import NavBar from './Nav-bar/NavBar'
import Services from './components/Services'
import Register from './components/Register';
import SignIn from './components/Signin';
import Home from './components/Home';
import NotFound from './components/NotFound';
import BookAService from './components/BookAService';
import AuthContext, { getCustomer } from './providers/AuthContext';
import ClimbingBoxLoader from 'react-spinners/ClimbingBoxLoader';
import CustomerTemplate from './components/CustomerTemplate';
import ForgottenPassword from './components/ForgottenPassword';
function App() {
  const [authValue, setAuthState] = useState({
    customer: getCustomer(),
    isLoggedIn: Boolean(getCustomer()),
  });

  const [loader, setLoader] = useState(false);

  useEffect(() => {
    setLoader(true);
    setTimeout(() => {
      setLoader(false);
    }, 2500);
  }, []);

  return (
    <Router>
      {loader ? (
        <div className="Loader">
          <ClimbingBoxLoader size={30} color={'#D2691E'} loader={loader} />
        </div>
      ) : (
        <div className="App">
          <AuthContext.Provider value={{ ...authValue, setAuthState }}>
            <NavBar />
            <Switch>
              <Redirect path='/' exact to={Home} />
              <Route path='/home' exact component={Home} />
              <Route path='/services' exact component={Services} />
              <Route path='/servicesbook' exact component={BookAService} />
              <Route path='/order' exact component={CustomerTemplate} />
              <Route path='/register' exact component={Register} />
              <Route path='/signin' exact component={SignIn} />
              <Route path='/forgotten' exact component={ForgottenPassword} />
              <Route path='*' exact component={NotFound} />
            </Switch>
          </AuthContext.Provider>
        </div>)}
    </Router>
  );
};
// GuardedRoute;  

export default App;
