import { Button } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import  { getCars } from "../common/dataProvider";
import { DataTableCars } from "./DataTable";

const EditCarsInterface = () => {
    const [data, setData] = useState([]);
    const [q, setQ] = useState('');
    const [isCarsButtonClicked, setIsCarsBtnClicked] = useState(false);

    const getData = async () => {
        let query;
        const result = await getCars(query);
        setData(result);
    };

    useEffect(() => {
            getData(q);
    }, [q]);

    return (
        <div>
            <Button type="button" variant='contained' color='black' className="carsButton" onClick={() => setIsCarsBtnClicked(!isCarsButtonClicked)}>Cars</Button>
            {isCarsButtonClicked
                ?
                <div>
                        {console.log(data, 'datatable')}
                        {
                            data && data.length > 0
                                ?
                                <DataTableCars data={data} />
                                :
                                null
                        }
                    </div>
                :
                ""
            }
        </div>
    )
};

export default EditCarsInterface;