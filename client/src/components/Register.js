import React, { useState } from 'react';
import { Link } from 'react-router-dom';  

const Register = () => {
  const [customer, setCustomer] = useState({
    firstname: '',
    lastname: '',
    email: '',
    phone: '',
  });
  const [error, setError] = useState('');

  const updateCustomer = (prop, value) => {
    setCustomer({
      ...customer,
      [prop]: value,
    });
  }

  const register = () => {
    if(customer.firstname && customer.lastname && customer.email && customer.phone) {
    fetch(`http://localhost:5555/customers/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(customer),
    })
    .then(r => r.json())
    .then((data) => {
      if(data.error) {
        const errors = Object.keys(data.response).map(er => data.response[er]);
        
        setError(errors.join('\n'));
      }
    })
    .catch(error => setError(error.toString()));
  }
  if(!customer.firstname) {
    setError('Enter a valid firstname!');
  }
  if(!customer.lastname) {
    setError('Enter a valid lastname!');
  }
  if(!customer.email) {
    setError('Enter a valid email!');
  }
  if(!customer.phone) {
    setError('Enter a valid phone number')
  }
  if(!customer.firstname && !customer.lastname) {
    setError('Enter a valid firstname and lastname!');
  }
  if(!customer.firstname || !customer.lastname || !customer.email || !customer.phone ) {
    setError('Please fill all the fields')
  }

  if(customer.email === Object.values(customer)) {
    setError('Email already exists!');
  }
}

  return (
    <div className="register">
      <h2>Register Form</h2>
      <p>Enter the details belows to make an account</p>
      <label className="side-label">
        <span> Firstname </span>
        <input type="text" placeholder=" Your firstname" value={customer.firstname} name='firstname' onChange={e => updateCustomer('firstname', e.target.value)} />
        <br/>
      </label>
      <label className="side-label">
        <span> Lastname </span>
        <input type="text" placeholder=" Your lastname" value={customer.lastname} name='lastname' onChange={e => updateCustomer('lastname', e.target.value)} />
        <br/>
      </label>
      <label className="side-label">
        <span> Email </span>
        <input type="email" placeholder=" Your email" value={customer.email} name='email' onChange={e => updateCustomer('email', e.target.value)} />
        <br/>
      </label>
      <label className="side-label">
        <span> Phone </span>
        <input type="phone" placeholder="Your phone number" value={customer.phone} name='phone' onChange={e => updateCustomer('phone', e.target.value)} />
        <br/>
      </label>
      <label className="select-label">
          <span className="select-label-text">
            You already have an accout? Click
              <Link to={'/signin'}> HERE </Link>
             to sign in!
          </span>
        </label>
        <div className="error">
          <pre>{error.toString()}</pre>
        </div>
        <button onClick={register}>Register</button>
      </div>
  );
};
export default Register;
