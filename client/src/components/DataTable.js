import React, { useState } from 'react';
import { DataGrid } from '@material-ui/data-grid';
import { Button, makeStyles, MenuItem, Select } from '@material-ui/core';
import { Formik } from 'formik';
import * as yup from 'yup';
export const DataTableCustomers = ({ data }) => {
    const [deletedRows, setDeletedRows] = useState([]);
    const [rows, setRows] = useState(data)
    const [isCreateBtnClicked, setIsCreateBtnClicked] = useState(false);

    const columns = [
        { field: 'id', hide: 'true' },
        { field: 'firstname', editable: 'true', headerName: 'Firstname', headerClassName: 'customers-table-header', headerAlign: 'center', width: 140 },
        { field: 'lastname', editable: 'true', headerName: 'Sirname', headerClassName: 'customers-table-header', headerAlign: 'center', width: 140 },
        { field: 'email', editable: 'true', headerName: 'Email address', headerClassName: 'customers-table-header', headerAlign: 'center', width: 180 },
        { field: 'phone', editable: 'true', headerName: 'Phone number', headerClassName: 'customers-table-header', headerAlign: 'center', width: 170 },
    ];

    const useStyles = makeStyles({
        cell: {
            backgroundColor: '#ffa07a',
        },
        root: {
            '& .customers-table-header': {
                backgroundColor: '#808082',
            },
        },
    });

    const createCustomer = () => {
        return (
            <div>
                <Formik
                    initialValues={{
                        firstname: '',
                        lastname: '',
                        email: '',
                        phone: '',
                    }}
                    validationSchema={yup.object({
                        email: yup
                            .string('Enter an email')
                            .email('Enter a valid email')
                            .required('Email is required'),
                        firstname: yup
                            .string('Enter firstname and sirname')
                            .matches(/^[aA-zZ\s]+$/, 'Only alphabets are allowed for this field ')
                            .min(3, 'Name should be of minimum 3 characters long')
                            .max(20, 'Name should be of maximum 20 characters long')
                            .required('Firstname is required'),
                        lastname: yup
                            .string('Enter firstname and sirname')
                            .matches(/^[aA-zZ\s]+$/, 'Only alphabets are allowed for this field ')
                            .min(3, 'Name should be of minimum 3 characters long')
                            .max(20, 'Name should be of maximum 20 characters long')
                            .required('Sirname is required'),
                        phone: yup
                            .string('Enter a phone number')
                            .matches(/^0\d{7,9}$/, 'Phone number should start with 0 and be with total of 10 numbers')
                            .required('Phone number is required')
                    })}
                    onSubmit={(values, onSubmitProps) => {
                        fetch(`http://localhost:5555/customers/register`, {
                            method: "POST",
                            headers: {
                                "Authorization": "Bearer " + localStorage.getItem("token"),
                                "Content-Type": "application/json",
                            },
                            body: JSON.stringify(values)
                        }).then(res => res.json())
                            .then(data => {
                                if (data.error) {
                                    throw new Error(data.error);
                                };
                                onSubmitProps.setSubmitting(false);
                                onSubmitProps.resetForm();
                                setRows([...rows, data]);
                            })
                            .catch(error => console.log(error.toString()));
                    }
                    }>
                    {props => (
                        <form onSubmit={props.handleSubmit} left='10px'>
                            <div>
                                Firstname:
                        <input
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.firstname}
                                    name="firstname"
                                />
                            </div>

                            <div>
                                Sirname:
                        <input
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.lastname}
                                    name="lastname"
                                />
                            </div>

                            <div>
                                Email:
                        <input
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.email}
                                    name="email"
                                />
                            </div>
                            <div>
                                Phone number:
                        <input
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.phone}
                                    name="phone"
                                />
                            </div>
                            {props.errors.firstname && <div id="feedback" style={{ color: 'red' }}>{props.errors.firstname}</div>}
                            {props.errors.lastname && <div id="feedback" style={{ color: 'red' }}>{props.errors.lastname}</div>}
                            {props.errors.email && <div id="feedback" style={{ color: 'red' }}>{props.errors.email}</div>}
                            {props.errors.phone && <div id="feedback" style={{ color: 'red' }}>{props.errors.phone}</div>}
                            <button type="submit" disabled={!props.isValid || props.isSubmitting}>Submit</button>
                        </form>
                    )}
                </Formik>
            </div >
        )
    }

    const handleRowSelection = (e) => {
        if (e.isSelected) {
            setDeletedRows([...deletedRows, ...rows.filter((r) => r.id === e.data.id)]);
        } else {
            setDeletedRows([...deletedRows.filter((r) => r.id === e.data.id)]);
        }
    };

    const deleteSelectedCustomers = async () => {
        const promises = deletedRows.map((row) => deleteCustomer(row.id));
        await Promise.all(promises);
    };

    const deleteCustomer = async (id) => {
        let data;
        try {
            const res = await fetch(`http://localhost:5555/customers/${id}`, {
                method: "DELETE",
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem("token")
                }

            });

            data = await res.json();

        } catch (err) {
            console.log(err.toString());
        }
        if (data.error) {
            throw new Error(data.error);
        }
        setRows(
            rows.filter((r) => deletedRows.filter((sr) => sr.id === r.id).length < 1)
        );
    }

    const editCustomer = async (id, customer) => {
        let data;
        try {
            const res = await fetch(`http://localhost:5555/customers/${id}`, {
                method: "PUT",
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem("token"),
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(customer),
            });

            data = await res.json();
        } catch (err) {
            console.log(err.toString());
        };
        return data;
    };

    const handleEditCellChangeCommitted = async (e) => {

        console.log(e, 'edit')
        const row = rows.find((row) => row.id === e.id);

        // Using Oject.assign() in order to replace the edited value with the old one
        const updatedRow = Object.assign(row, { [e.field]: e.props.value });

        // const [firstname, lastname] = updatedRow.fullname.split(' ');

        const customer = {
            firstname: updatedRow.firstname,
            lastname: updatedRow.lastname,
            email: updatedRow.email,
            phone: updatedRow.phone
        };
        console.log(customer)
        await editCustomer(e.id, customer);
    };

    return (
        <div className='table'>
            <h1 className='customers-header'>Customers</h1>
            <Button variant='contained' color='secondary' className='delete-button' onClick={deleteSelectedCustomers} size="small">
                Delete
                </Button>
            <Button variant='contained' color='primary' className='create-button' onClick={() => setIsCreateBtnClicked(!isCreateBtnClicked)} size="small">
                Create
                </Button>
            {isCreateBtnClicked
                ?
                <div className="createCustomer">
                    {createCustomer()}
                </div>
                :
                ''}
            <br />
            <div className='table-container' style={{diplay: 'flex', justifyContent: 'center'}}>
            <div style={{ height: 400, width: '680px', textAlign: 'center' }} className={useStyles().root}>
                <DataGrid
                    getRowId={(data) => data.id}
                    checkboxSelection
                    rows={rows}
                    columns={columns}
                    pageSize={5}
                    onRowSelected={handleRowSelection}
                    onEditCellChangeCommitted={handleEditCellChangeCommitted}
                />
            </div>
            </div>
        </div>
    );
};

export const DataTableOrders = ({ data }) => {
    const [deletedRows, setDeletedRows] = useState([]);
    const [rows, setRows] = useState(data);
    const columns = [
        { field: 'id', hide: 'true' },
        { field: 'fullname', headerName: 'Fullname', editable: false, headerClassName: 'orders-table-header', headerAlign: 'center', width: 135 },
        { field: 'type', headerName: 'Service type', editable: false, headerClassName: 'orders-table-header', headerAlign: 'center', width: 155 },
        { field: 'labor_cost', headerName: 'Labor cost', editable: false, headerClassName: 'orders-table-header', headerAlign: 'center', width: 145 },
        { field: 'order_date', headerName: 'Date of order', editable: false, headerClassName: 'orders-table-header', type: 'dateTime', headerAlign: 'center', width: 160 },
        {
            field: 'status', headerName: 'Status of order', editable: true, headerClassName: 'orders-table-header', headerAlign: 'center',
            renderCell: (data) => (
                <strong>
                    <Select
                        labelId="demo-controlled-open-select-label"
                        id="demo-controlled-open-select"
                        value={data.value}
                        onChange={(e) => editOrdersStatus(data.id, e.target.value)}
                    >
                        <MenuItem value={'pending'}>Pending</MenuItem>
                        <MenuItem value={'ongoing'}>Ongoing</MenuItem>
                        <MenuItem value={'done'}>Done</MenuItem>
                    </Select>
                </strong>
            )
            , width: 175
        },
    ];

    const useStyles = makeStyles({
        root: {
            '& .orders-table-header': {
                backgroundColor: '#808082',
            },
        },
    });

    const handleRowSelection = (e) => {
        if (e.isSelected) {
            setDeletedRows([...deletedRows, ...rows.filter((r) => r.id === e.data.id)]);
        } else {
            setDeletedRows([...deletedRows.filter((r) => r.id === e.data.id)]);
        }
    };

    const editOrdersStatus = async (id, status) => {
        let data;
        try {
            const res = await fetch(`http://localhost:5555/orders/${id}`, {
                method: "PUT",
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem("token"),
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({status}),
            });

            data = await res.json();
        } catch (err) {
            console.log(err.toString());
        };

        return data;
        
    };

    return (
        <div className='table'>
            <h1 className='orders-heading'>Orders</h1>
            <div style={{ height: 400, width: '820px', textAlign: 'center' }} className={useStyles().root}>
                <DataGrid
                    getRowId={(data) => data.id}
                    checkboxSelection
                    rows={rows}
                    columns={columns}
                    pageSize={5}
                    onRowSelected={handleRowSelection}
                />
            </div>
        </div>
    );
};

export const DataTableServices = ({ data }) => {
    const [deletedRows, setDeletedRows] = useState([]);
    const [rows, setRows] = useState(data)
    const [isCreateBtnClicked, setIsCreateBtnClicked] = useState(false);

    const columns = [
        { field: 'id', hide: 'true' },
        { field: 'type', headerName: 'Service type', editable: 'true', headerClassName: 'services-table-header', headerAlign: 'center', width: 155 },
        { field: 'labor_cost', headerName: 'Labor cost', editable: 'true', headerClassName: 'services-table-header', headerAlign: 'center', width: 145 },
    ];

    const useStyles = makeStyles({
        root: {
            '& .services-table-header': {
                backgroundColor: '#808082',
            },
        },
    });

    const createService = () => {
        return (
            <div>
                <Formik
                    initialValues={{
                        type: '',
                        labor_cost: '',
                    }}
                    validationSchema={yup.object({
                        type: yup
                            .string('Enter a service type')
                            .min(3, 'Type should be of minimum 3 characters long')
                            .required('Service type is required'),
                        labor_cost: yup
                            .string('Enter the labor type for the current service')
                            .matches(/^\$[0-9]+\.?[0-9]?[0-9]?$/, 'You should add the dollar sign first - $ and number after it')
                            .required('Labor price is required'),
                    })}
                    onSubmit={(values, onSubmitProps) => {
                        fetch(`http://localhost:5555/services`, {
                            method: "POST",
                            headers: {
                                "Authorization": "Bearer " + localStorage.getItem("token"),
                                "Content-Type": "application/json",
                            },
                            body: JSON.stringify(values)
                        }).then(res => res.json())
                            .then(data => {
                                if (data.error) {
                                    throw new Error(data.error);
                                };
                                onSubmitProps.setSubmitting(false);
                                onSubmitProps.resetForm();
                            })
                            .catch(error => console.log(error.toString()));
                    }
                    }>
                    {props => (
                        <form onSubmit={props.handleSubmit}>
                            <div>
                                Service type:
                <input
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.type}
                                    name="type"
                                />
                            </div>

                            <div>
                                Labor price:
                <input
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.labor_cost}
                                    name="labor_cost"
                                />
                            </div>
                            {props.errors.type && <div id="feedback" style={{ color: 'red' }}>{props.errors.type}</div>}
                            {props.errors.labor_cost && <div id="feedback" style={{ color: 'red' }}>{props.errors.labor_cost}</div>}
                            <button type="submit" disabled={!props.isValid || props.isSubmitting}>Submit</button>
                        </form>
                    )}
                </Formik>
            </div>
        )
    }
    const handleRowSelection = (e) => {
        console.log(e);
        if (e.isSelected) {
            setDeletedRows([...deletedRows, ...rows.filter((r) => r.id === e.data.id)]);
        } else {
            setDeletedRows([...deletedRows.filter((r) => r.id === e.data.id)]);
        }
    };

    const deleteSelectedServices = async () => {
        const promises = deletedRows.map((row) => deleteService(row.id));
        await Promise.all(promises);
    };

    const deleteService = async (id) => {
        let data;
        try {
            const res = await fetch(`http://localhost:5555/services/${id}`, {
                method: "DELETE",
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem("token")
                }

            });

            data = await res.json();

        } catch (err) {
            console.log(err.toString());
        }
        if (data.error) {
            throw new Error(data.error);
        }
        setRows(
            rows.filter((r) => deletedRows.filter((sr) => sr.id === r.id).length < 1)
        );
    }

    const editService = async (id, customer) => {
        let data;
        try {
            const res = await fetch(`http://localhost:5555/services/${id}`, {
                method: "PUT",
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem("token"),
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(customer),
            });

            data = await res.json();
        } catch (err) {
            console.log(err.toString());
        };
        return data;
    };

    const handleEditCellChangeCommitted = async (e) => {

        const row = rows.find((row) => row.id === e.id);

        // Using Oject.assign() in order to replace the edited value with the old one
        const updatedRow = Object.assign(row, { [e.field]: e.props.value });

        const customer = {
            type: updatedRow.type,
            labor_cost: updatedRow.labor_cost,
        };
        await editService(e.id, customer);
    };
    return (
        <div className='table'>
            <h1>Services</h1>
            <Button variant='contained' color='secondary' className='delete-button' onClick={deleteSelectedServices} size="small">
                Delete
                </Button>
            <Button variant='contained' color='primary' className='create-button' onClick={() => setIsCreateBtnClicked(!isCreateBtnClicked)} size="small">
                Create
                </Button>
            {isCreateBtnClicked
                ?
                <div className="createService">
                    {createService()}
                </div>
                :
                ''}
            <br />
            <div style={{ height: 400, width: '350px', textAlign: 'center' }} className={useStyles().root}>
                <DataGrid
                    getRowId={(data) => data.id}
                    checkboxSelection
                    rows={rows}
                    columns={columns}
                    pageSize={5}
                    onRowSelected={handleRowSelection}
                    onEditCellChangeCommitted={handleEditCellChangeCommitted}
                />
            </div>
        </div>
    );
};


export const DataTableCars = ({ data }) => {
    const [deletedRows, setDeletedRows] = useState([]);
    const [rows, setRows] = useState(data);
    const columns = [
        { field: 'id', hide: 'true' },
        { field: 'brand', headerName: 'Brand', editable: false, headerClassName: 'cars-table-header', headerAlign: 'center', width: 115 },
        { field: 'model', headerName: 'Model', editable: false, headerClassName: 'cars-table-header', headerAlign: 'center', width: 115 },
        { field: 'vin', headerName: 'VIN', editable: true, headerClassName: 'cars-table-header', headerAlign: 'center', width: 170 },
        { field: 'registration_plate', headerName: 'Registration plate', headerClassName: 'cars-table-header', headerAlign: 'center', editable: true, width: 190 },
        { field: 'year', headerName: 'Production year', editable: true, headerClassName: 'cars-table-header', headerAlign: 'center', width: 180 },
        { field: 'fullname', headerName: 'Owner', editable: false, headerClassName: 'cars-table-header', headerAlign: 'center', width: 150 },
    ];

    const useStyles = makeStyles({
        root: {
            '& .cars-table-header': {
                backgroundColor: '#808082',
            },
        },
    });

    const handleRowSelection = (e) => {
        if (e.isSelected) {
            setDeletedRows([...deletedRows, ...rows.filter((r) => r.id === e.data.id)]);
        } else {
            setDeletedRows([...deletedRows.filter((r) => r.id === e.data.id)]);
        }
    };


    const editCar = async (id, car) => {
        let data;
        try {
            const res = await fetch(`http://localhost:5555/cars/${id}`, {
                method: "PUT",
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem("token"),
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(car),
            });

            data = await res.json();
        } catch (err) {
            console.log(err.toString());
        };
        return data;
    };

    const handleEditCellChangeCommitted = async (e) => {
        const row = rows.find((row) => row.id === e.id);

        // Using Oject.assign() in order to replace the edited value with the old one
        const updatedRow = Object.assign(row, { [e.field]: e.props.value });

        const car = {
            vin: updatedRow.vin,
            registration_plate: updatedRow.registration_plate,
            year: updatedRow.year
        };
        console.log(car)
        await editCar(e.id, car);
    };
    return (
        <div className='table'>
            <h1>Cars</h1>
            <div style={{ height: 400, width: '970px', textAlign: 'center' }} className={useStyles().root}>
                <DataGrid
                    getRowId={(data) => data.id}
                    checkboxSelection
                    rows={rows}
                    columns={columns}
                    pageSize={5}
                    onRowSelected={handleRowSelection}
                    onEditCellChangeCommitted={handleEditCellChangeCommitted}
                />
            </div>
        </div>
    );
};

export const DataTableReport = ({ data }) => {
    const [rows, setRows] = useState(data);
    const columns = [
        { field: 'id', hide: 'true' },
        { field: 'fullname', headerName: 'Owner', editable: false, headerClassName: 'report-table-header', headerAlign: 'center', width: 130 },
        { field: 'email', headerName: 'Email Address', editable: false, headerClassName: 'report-table-header', headerAlign: 'center', width: 130 },
        { field: 'phone', headerName: 'Phone Number', editable: false, headerClassName: 'report-table-header', headerAlign: 'center', width: 130 },
        { field: 'brand', headerName: 'Brand', editable: false, headerClassName: 'report-table-header', headerAlign: 'center', width: 115 },
        { field: 'model', headerName: 'Model', editable: false, headerClassName: 'report-table-header', headerAlign: 'center', width: 115 },
        { field: 'vin', headerName: 'VIN', editable: true, headerClassName: 'report-table-header', headerAlign: 'center', width: 170 },
        { field: 'registration_plate', headerName: 'Registration plate', editable: true, headerClassName: 'report-table-header', headerAlign: 'center', width: 130 },
        { field: 'year', headerName: 'Production year', editable: true, headerClassName: 'report-table-header', headerAlign: 'center', width: 130 },
        { field: 'service_type', headerName: 'Service type', editable: true, headerClassName: 'report-table-header', headerAlign: 'center', width: 130 },
        { field: 'labor_cost', headerName: 'Labor cost', editable: true, headerClassName: 'report-table-header', headerAlign: 'center', width: 145 },
    ];

    const useStyles = makeStyles({
        root: {
            '& .report-table-header': {
                backgroundColor: '#808082',
            },
        },
    });

    return (
        <div className='table'>
            <h1>Report</h1>
            <div style={{ height: 400, width: '1350px', textAlign: 'center' }} className={useStyles().root}>
                <DataGrid
                    getRowId={(data) => data.id}
                    checkboxSelection
                    rows={rows}
                    columns={columns}
                    pageSize={5}
                />
            </div>
        </div>
    );
}

export const DataTableReservations = ({ data }) => {
    const [rows, setRows] = useState(data);
    const columns = [
        { field: 'id', hide: 'true' },
        { field: 'model', headerName: 'Model', editable: false, headerClassName: 'reservation-table-header', headerAlign: 'center', width: 130 },
        { field: 'brand', headerName: 'Brand', editable: false, headerClassName: 'reservation-table-header', headerAlign: 'center', width: 130 },
        { field: 'service_type', headerName: 'Service type', editable: false, headerClassName: 'reservation-table-header', headerAlign: 'center', width: 160 },
        { field: 'vin', headerName: 'VIN', editable: false, headerClassName: 'reservation-table-header', headerAlign: 'center', width: 170 },
        { field: 'year', headerName: 'Production year', editable: false, headerClassName: 'reservation-table-header', headerAlign: 'center', width: 180 },
        { field: 'name', headerName: 'Fullname', editable: false, headerClassName: 'reservation-table-header', headerAlign: 'center', width: 140 },
        { field: 'email', headerName: 'Email address', editable: false, headerClassName: 'reservation-table-header', headerAlign: 'center', width: 160 },
        { field: 'phone', headerName: 'Phone Number', editable: false, headerClassName: 'reservation-table-header', headerAlign: 'center', width: 190 },
    ];

    const useStyles = makeStyles({
        root: {
            '& .reservation-table-header': {
                backgroundColor: '#808082',
            },
        },
    });

    return (
        <div className='table'>
            <h1 className='reservations-header'>Reservations</h1>
            <div style={{ height: 400, width: '1310px', textAlign: 'center' }} className={useStyles().root}>
                <DataGrid
                    getRowId={(data) => data.id}
                    checkboxSelection
                    rows={rows}
                    columns={columns}
                    pageSize={5}
                />
            </div>
        </div>
    );
}