import React from 'react';
import 'react-datepicker/dist/react-datepicker.css'
import EditCustomersInterface from './EditCustomersButtonInterface';
import EditServicesInterface from './EditServicesButtonInterface';
import EditCarsInterface from './EditCarsButtonInterface';
import EditOrdersInterface from './EditOrdersButtonInterface';
import GetFullReport from './GetFullReport';
import GetReservations from './EditReservationsButtonInterface';

const AdminPanel = () => {
    return (
        <div className='admin-panel-components' style={{backgroundImage: `url('http://localhost:3000/admin-pannel.jpg')`, width:'250vh', height:'220vh', backgroundSize: 'cover'}}>
            <h1 className='admin-panel-heading'>Admin Panel</h1>
            <EditCustomersInterface />
            <GetFullReport />
            <EditServicesInterface />
            <EditOrdersInterface />
            <EditCarsInterface />
            <GetReservations />
        </div>
    )

};

export default AdminPanel;