import { useEffect, useState } from "react";
import { getCustomer } from "../providers/AuthContext";
//import ClimbingBoxLoader from 'react-spinners/ClimbingBoxLoader';
import AdminPanel from "./AdminPage";
import CustomerTemplate from "./CustomerTemplate";
import HomeInterface from "./HomeInterface";


const HomePage = (props) => {
    const [customer, setCustomer] = useState({
        email: '',
        password: '',
        role: '',
    });
    useEffect(() => {
            const token = localStorage.token;
            const customer = getCustomer(token);
            if (customer) {
                setCustomer(customer);

            } 
            // else {
            //     props.history.push('/');

            // }

    }, [props.history]);

    return (
        <>
        {customer.role === 'admin' 
        ?
        <AdminPanel/>
        : 
        <HomeInterface /> 
        }
        </>
    );
}

export default HomePage;