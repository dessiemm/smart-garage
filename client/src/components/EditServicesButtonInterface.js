import { Button } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { getServices } from "../common/dataProvider";
import { DataTableServices } from "./DataTable";

const EditServicesInterface = () => {
    const [data, setData] = useState([]);
    const [q, setQ] = useState('');
    const [isServicesBtnClicked, setIsServicesBtnClicked] = useState(false);

    const getData = async () => {
        let query;
        const result = await getServices(query);
        setData(result);
    };

    useEffect(() => {
            getData(q);
    }, [q]);

    return (
        <div>
            <Button type="button" variant='contained' color='black' className="servicesBtn" onClick={() => setIsServicesBtnClicked(!isServicesBtnClicked)}>Services</Button>
            {isServicesBtnClicked
                ?
                <div>
                    <div>
                        {console.log(data, 'datatable')}
                        {
                            data && data.length > 0
                                ?
                                <DataTableServices data={data} />
                                :
                                null
                        }
                        </div>
                    </div>
                :
                ""
            }
        </div>
    )
};

export default EditServicesInterface;