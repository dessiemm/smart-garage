import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';


const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    root: {
        height: "100%",
        backgroundImage: `url('http://localhost:3000/light.gif')`,    
      },
}));
const BookAService = () => {
    const [carList, setCars] = useState([]);
    const [brandList, setBrands] = useState([]);
    const [modelList, setModels] = useState([]);
    const [serviceList, setServices] = useState([]);
    const classes = useStyles();

    useEffect(() => {
        fetch(`http://localhost:5555/brands`, {
            // headers: {
            //     "Authorization": "Bearer " + localStorage.getItem("token")
            // }
        })
            .then(response => response.json())
            .then(result => {
                if (result.error) {
                    throw new Error(result.error)
                }
                const newResult = result.map((el, index) => ({
                    ...el,
                    value: index+1
                }))
                setBrands(newResult);
            })
            .catch(error => {
                // setError(error.message);
            });
        fetch(`http://localhost:5555/services`, {
            // headers: {
            //     "Authorization": "Bearer " + localStorage.getItem("token")
            // }
        })
            .then(response => response.json())
            .then(result => {
                if (result.error) {
                    throw new Error(result.error)
                }
                setServices(result);
            })
            .catch(error => {
                // setError(error.message);
            });
    }, []);
    const [brand,setBrand]= useState(null);
    const [model, setModel] = useState(null);
    const [service, setService] = useState(null);
    const [vin, setVin] = useState('');
    const [year, setYear] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    
    const handleSubmit = (event) => {
        event.preventDefault();
        const dataToSend = {
            brand_id: brand,
            model_id: model,
            service_id: service,
            vin,
            year,
            name,
            email,
            phone
        };
    
           console.log(dataToSend)
        fetch(`http://localhost:5555/reservations`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(dataToSend)
        })
            .then(response => response.json())
            .then(result => {
                console.log(result, 'response')
                if (result.error) {
                    throw new Error(result.error)
                }
                console.log('result', result);
                setBrand(null);
                setModel(null);
                setService(null);
                setVin('');
                setYear('');
                setName('');
                setPhone('');
                setEmail('');
            })
            .catch(error => {
                console.log(error);
            })
        }
    return (
        <div className='formbook' style={{backgroundImage: `url('http://localhost:3000/service.jpg')`, width: '205.3vh', height:'100vh', backgroundSize: "cover"}}>
        <form style={{width: '500px', height: '200px', display: 'flex', flexDirection: 'column', justifyContent: 'space-between'}}
        onSubmit={handleSubmit}>
            <div  className='form-book-service'>
            <div>
                <FormControl>
                    <InputLabel id="demo-dialog-select-label">Brand</InputLabel>
                    <Select
                        labelId="demo-dialog-select-label"
                        id="demo-dialog-select"
                        value={brand}
                        onChange={(event) => {
                            setBrand(event.target.value);
                            setModel(null);
                            fetch(`http://localhost:5555/models/${event.target.value}`, {
                                // headers: {
                                //     "Authorization": "Bearer " + localStorage.getItem("token")
                                // }
                            })
                                .then(response => response.json())
                                .then(result => {
                                    if (result.error) {
                                        throw new Error(result.error)
                                    }
                                    const newResult = result.map((el, index) => ({
                                        ...el,
                                        value: index+1
                                    }));
                                    setModels(newResult);
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                        }}
                        input={<Input />}
                    >
                        {brandList.length > 0 && brandList.map((el) => <MenuItem value={el.value} key={el.value}>{el.brand}</MenuItem>)}
                    </Select>
                </FormControl>
            </div>
            <div>
                <FormControl>
                    <InputLabel id="demo-dialog-select-label">Models</InputLabel>
                    <Select
                        labelId="demo-dialog-select-label"
                        id="demo-dialog-select"
                        value={model}
                        onChange={(event) => {
                            setModel(event.target.value);
                        }}
                        input={<Input/>}
                    >
                        {modelList.length > 0 && modelList.map((el) => <MenuItem value={el.value} key={el.value}>{el.model}</MenuItem>)}
                    </Select>
                </FormControl>
            </div>
            <div>
                <FormControl>
                    <InputLabel id="demo-dialog-select-label">Services</InputLabel>
                    <Select
                        labelId="demo-dialog-select-label"
                        id="demo-dialog-select"
                        value={service}
                        onChange={(event) => {
                            setService(event.target.value);
                        }}
                        input={<Input />}
                    >
                        {serviceList.length > 0 && serviceList.map((el) => <MenuItem value={el.id} key={el.id}>{el.type}</MenuItem>)}
                    </Select>
                </FormControl>
            </div>
            <div  className='letters'>
                Vin               
                 <input className='text' type="text" value={vin} onChange={(event) => setVin(event.target.value)} />
            </div>
            <div className='letters'>
                Year
                <input  className='text' type="text" value={year} onChange={(event) => setYear(event.target.value)} />
            </div>
            <div className='letters'>
                Name
                <input  className='text' type="text" value={name} onChange={(event) => setName(event.target.value)} />
            </div>
            <div className='letters'>
                Email
                <input  className='text' type="text" value={email} onChange={(event) => setEmail(event.target.value)} />
            </div>
            <div className='letters'>
                Phone
                <input  className='text' type="text" value={phone} onChange={(event) => setPhone(event.target.value)} />
            </div>
            <div>
            <button className='button1' type="submit" onClick={handleSubmit}>Submit</button>
            </div>
            </div>
        </form>
        </div>
    )}
export default BookAService;