import { NavLink } from "react-router-dom";

const HomeInterface = () => {
    return (
        <div className="home-interface" style={{ backgroundImage: `url('http://localhost:3000/home-wallpaper.jpg')`, width: '205.4vh', height: '220vh', backgroundSize: 'cover' }}>
            <div className="content-home">
                <h1 className="title">Welcome to Smart Garage</h1>
                <p className="subTitle" >Car service website, where you can leave your vechicle in SAFE HANDS!</p>
                <p className="subTitle">In order to experience all the services our site provides, better be logged in.</p>
            </div>
            <label className="select-label-home">
                <span className="select-label-text-home">
                    You don't have an accout? Click
              <NavLink to='/register'> HERE </NavLink>
             to make one!
                </span>
            </label>
            <div className="paragraphs">
                <ul>
                    <li>You can learn about the services we offer<NavLink to='/services' activeStyle={{ color: 'red' }}> HERE</NavLink></li>
                    <li>You can book a service<NavLink to='/servicesbook'> HERE</NavLink></li>
                    <li>If you are logged in and have a car under repair, you can check the status<NavLink to='/order'> HERE</NavLink></li>
                </ul>
            </div>
            <h2 className='about-us'>About Us:</h2>
            <div className="row">
                <div className="column">
                    <div className="card">
                        <img src="team1.jpg" alt="Desislava" style={{width:'100%'}}/>
                            <div className="container">
                                <h2>Desislava Milanova</h2>
                                <p className="titleDesi">CEO & Founder</p>
                                <p>Managing the accounting of the company.</p>
                                <p>desislava_b_milanova@abv.bg</p>
                                <p>Contact: +359 879595965</p>
                            </div>
                        </div>
                    </div>

                    <div className="column">
                        <div className="card2">
                            <img src="team2.jpg" alt="Mihail" style={{width:'100%'}}/>
                                <div className="container">
                                    <h2>Mihail Enev</h2>
                                    <p className="titleMisho">CEO & Founder</p>
                                    <p>Managing the general parts orders and leading the workers</p>
                                    <p>mcenev@abv.bg</p>
                                    <p>Contact: +359 885803434</p>
                                </div>
                            </div>
                        </div>

                        <div className="column">
                            <div className="card3">
                                <img src="team3.jpg" alt="Telerik" style={{width:'100%'}}/>
                                    <div className="container">
                                        <br></br>
                                        <br></br>
                                        <br></br>
                                        <h2>Telerik Academy</h2>
                                        <p className="titleTelerik">Motivators</p>
                                        <p>Maintainers of the website</p>
                                        <p>info@telerikacademy.com</p>
                                        <p>Contact them by the email above...</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
    );
};

export default HomeInterface;