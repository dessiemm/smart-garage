import { Button } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { getReport } from "../common/dataProvider";
import { DataTableReport } from "./DataTable";

const GetFullReport = () => {
    const [data, setData] = useState([]);
    const [q, setQ] = useState('');
    const [isGetReportBtnClicked, setIsGetReportBtnClicked] = useState(false);

    const getData = async (searchValue) => {
        let query;
        const result = await getReport(query);
        setData(result);
    };

    useEffect(() => {
        getData(q)
    }, [q]);


    return (
        <div>
            <Button type="button" variant='contained' color='black' className='reportBtn' onClick={() => setIsGetReportBtnClicked(!isGetReportBtnClicked)}>Get full report</Button>
            {isGetReportBtnClicked
                ?
                <div>
                    <div>
                        {console.log(data, 'datatable Рерпорт')}
                        {
                            data && data.length > 0
                                ?
                                <DataTableReport data={data} />
                                :
                                null
                        }
                    </div>
                </div> 
                : ''}
                </div>
    )
};

export default GetFullReport;