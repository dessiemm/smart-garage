import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import decode from 'jwt-decode';
import AuthContext from '../providers/AuthContext';

const SignIn = (props) => {
  const [customer, setCustomer] = useState({
    email: '',
    password: '',
  });

  const [error, setError] = useState('');

  const auth = useContext(AuthContext);

  const updateCustomer = (prop, value) => {
    setCustomer({
      ...customer,
      [prop]: value,
    });
  };

  const login = () => {
    if(customer.email && customer.password) {
    fetch(`http://localhost:5555/customers/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(customer),
    })
      .then(r => r.json())
      .then((data) => {
        if(data.errors) {
          const errorMessage = typeof data.errors === 'object' ? Object.values(data.errors).join('\n') : data.errors;
            setError(errorMessage);
            return;  
          }
        try {
          const customer = decode(data.token);
          localStorage.setItem('token', data.token);
          auth.setAuthState({ customer, isLoggedIn: true });
          props.history.push('/home');
        }
        catch (error) {
            console.log(error);
          }
      })
      .catch(error => setError(error.toString()));
  } 
  if(!customer.email) {
    setError('Enter a valid password!');
  }
  if(!customer.password) {
    setError('Enter a valid email!');
  }
  if(!customer.password && !customer.email) {
    setError('Enter your email and password!')
  }
};

  return (
    <>
      <div className="signin">
        <h2>Signin Form</h2>
        <p>Enter your info to sign in your account</p>
        <label className="side-label">
          <span>Email </span>
          <input type="text" placeholder="Your email" value={customer.email} onChange={e => updateCustomer("email", e.target.value)} />
          <br/>
        </label>
        <label className="side-label">
          <span>Password </span>
          <input type="password" placeholder="Your password" value={customer.password} onChange={e => updateCustomer('password', e.target.value)} />
          <br/>
        </label>
        <label className="select-label">
          <span className="select-label-text">
            You don't have an accout? Click
              <Link to={'/register'}> HERE </Link>
             to make one!
          </span>
        </label>
        <div className="error">
          <pre>{error.toString()}</pre>
        </div>
        <button onClick={login}>Sign in</button>
        <p className='forgotten-password text-right'>
          <Link to={'/forgotten'}>Forgot password? </Link>
        </p>
      </div>
    </>
  )
};


export default SignIn;
