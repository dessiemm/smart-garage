import { useEffect, useState } from "react";
import AuthContext from "../providers/AuthContext";
import { getCustomer } from "../providers/AuthContext";
import { Link } from 'react-router-dom';
import moment from "moment";

const CustomerTemplate = () => {
    const [data, setData] = useState([]);
    const [authValue, setAuthState] = useState({
        customer: getCustomer(),
        isLoggedIn: Boolean(getCustomer()),
    });
    useEffect(() => {
        const customer = getCustomer();
        fetch(`http://localhost:5555/orders/${customer.id}`, {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("token")
            }
        })
            .then(response => response.json())
            .then(result => {
                setData(result);
                if (result.error) {
                    throw new Error(result.error)
                }
                console.log(result)
            })
            .catch(error => {
                console.log(error)
            });
    }, [])

    return (
        <div className='customertemplate' style={{ backgroundImage: `url('http://localhost:3000/background.jpg')`, width: '211vh', height: '100vh', backgroundSize: "cover" }}>
            {authValue.isLoggedIn
                ? 
                (<>
                <h1 className='orders-header'>Your Current Status:</h1>
                    {data.length > 0 ? data.map((el) => (
                        <div className='card-order'>
                        <div className='order'>
                            Fullname: {el.fullname}
                        </div>
                        <div className='order'>
                            Price: {el.labor_cost}
                        </div>
                        <div className='order'>
                            Date of order: {moment(el.order_date).format('MMMM Do YYYY')}
                        </div>
                        <div className='order'>
                            Order status: {el.status}
                        </div>
                        <div className='order'> 
                            Registration plate: {el.registration_plate}
                        </div>
                        <div className='order'>
                            Service type: {el.type}
                        </div>
                        </div>
                    )) : <h1 className='order-message'>You do not have vechicle under repair at the moment...</h1>}
                </>)
                : (
                    <div>
                        <h1>You have to sign in to access this page!</h1>
                        <Link className='buttonsign' to="/signin"> CLICK HERE to sign in </Link>
                    </div>
                )
            }
        </div>
    )
};

export default CustomerTemplate;