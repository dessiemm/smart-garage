import { Button } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import  { getOrders } from "../common/dataProvider";
import { DataTableOrders } from "./DataTable";
 
const EditOrdersInterface = () => {
    const [data, setData] = useState([]);
    const [q, setQ] = useState('');
    const [isOrdersButtonClicked, setIsOrdersButtonClicked] = useState(false);

    const getData = async () => {
        let query;
        const result = await getOrders(query);
        setData(result);
    };

    useEffect(() => {
            getData(q);
    }, [q]);

    return (
        <div>
            <Button type="button" variant='contained' color='black' className="ordersButton" onClick={() => setIsOrdersButtonClicked(!isOrdersButtonClicked)}>Orders</Button>
            {isOrdersButtonClicked
                ?
                <div>
                        {console.log(data, 'datatable')}
                        {
                            data && data.length > 0
                                ?
                                <DataTableOrders data={data} />
                                :
                                null
                        }
                    </div>
                :
                ""
            }
        </div>
    )
};

export default EditOrdersInterface;