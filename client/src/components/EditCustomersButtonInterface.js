import { Button } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import { getCustomers } from '../common/dataProvider';
import { DataTableCustomers } from './DataTable';

const EditCustomersInterface = () => {

    const [data, setData] = useState([]);
    const [q, setQ] = useState('');
    const [isCustomersBtnClicked, setIsCustomersBtnClicked] = useState(false);

    const getData = async () => {
        let query;
        const result = await getCustomers(query);
        setData(result);
    };

    useEffect(() => {
        getData(q)
    }, [q]);


    return (
        <div>
            <Button type="button"  variant='contained' color='black' className='customersBtn' onClick={() => setIsCustomersBtnClicked(!isCustomersBtnClicked)}>Customers</Button>
            {isCustomersBtnClicked
                ?
                <div>
                    <div>
                        {console.log(data, 'datatable')}
                        {
                            data && data.length > 0
                                ?
                                <DataTableCustomers data={data} />
                                :
                                null
                        }
                    </div>
                </div>
                : ''}
        </div>
    )
};
export default EditCustomersInterface;