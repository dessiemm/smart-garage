import { Button } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { getReservations } from "../common/dataProvider";
import { DataTableReservations } from "./DataTable";

const GetReservations = () => {
    const [data, setData] = useState([]);
    const [q, setQ] = useState('');
    const [isGetReservationsBtnClicked, setIsReservationsBtnClicked] = useState(false);

    const getData = async () => {
        let query;
        const result = await getReservations(query);
        setData(result);
    };

    useEffect(() => {
        getData(q)
    }, [q]);


    return (
        <div>
            <Button type="button" variant='contained' color='black' className='reservationsBtn' onClick={() => setIsReservationsBtnClicked(!isGetReservationsBtnClicked)}>Reservations</Button>
            {isGetReservationsBtnClicked
                ?
                <div>
                    <div>
                        {console.log(data, 'datatable reserv')}
                        {
                            data && data.length > 0
                                ?
                                <DataTableReservations data={data} />
                                :
                                null
                        }
                    </div>
                </div> 
                : ''}
                </div>
    )
};

export default GetReservations;