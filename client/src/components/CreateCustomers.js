import React, { useContext, useEffect, useState} from 'react';
import AuthContext from '../providers/AuthContext';

const CreateCustomer = () => {
    const [customersList, setCustomersList] = useState([]);
    const [firstname, setFirstname] = useState('');
    const [lastname, setLastname] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [phone, setPhone] = useState('');
    const [role, setRole] = useState('');

    const createCustomer = () => {
        const dataToSend = {
            firstname: firstname,
            lastname: lastname,
            email: email,
            password: password,
            phone: phone,
            role: role,
        };

        fetch(`http://localhost:5555/register`, {
            method: "POST",
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("token"),
                "Content-Type": "application/json",
            },
            body: JSON.stringify(dataToSend),
        })
        .then(res => res.json())
        .then(data => {
            if (data.error) {
                throw new Error(data.error);
            }
            fetch()
        })

    }
}
