// import { Button } from '@material-ui/core';
import React, { useState } from 'react';
// import { Link } from 'react-router-dom';
// import AuthContext from '../providers/AuthContext';

const ForgottenPassword = () => {
    const [customer, setCustomer] = useState({
        email: '',
    });

    const [error, setError] = useState('');

    const updateCustomer = (prop, value) => {
        setCustomer({
          ...customer,
          [prop]: value,
        });
      };
    
    const forgotten = () => {
        fetch(`http://localhost:5555/customers/forgotten`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(customer),
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                if (data.errors) {
                    const errorMessage = typeof data.errors === 'object' ? Object.values(data.errors).join('\n') : data.errors;
                    setError(errorMessage);
                    return;
                }   
            })
            .catch(err => console.log(err));
    };

    return (
        <div>
            <>
                <div className="forgotten">
                    <h2>Forgotten Password</h2>
                    <p>Enter your email to receive new password</p>
                    <label className="side-label">
                        <span>Email</span>
                        <input type="text" placeholder="Your email" value={customer.email} onChange={e => updateCustomer('email', e.target.value)} />
                        <br />
                    </label>
                    <div className="error">
                        <pre>{error.toString()}</pre>
                    </div>
                    {/* <div className="error">
                        <pre>{error.toString()}</pre>
                    </div> */}
                    <button onClick={forgotten}>Submit</button>
                </div>
            </>
        </div>
    )
};

export default ForgottenPassword;