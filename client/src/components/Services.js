import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
//import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    textAlign: 'center',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));
const Services = () => {
  const [services, setServices] = useState([]);
  useEffect(() => {
    fetch(`http://localhost:5555/services`, {
      headers: {
        "Authorization": "Bearer " + localStorage.getItem("token")
      }
    })
      .then(response => response.json())
      .then(result => {
        if (result.error) {
          throw new Error(result.error)
        }
        setServices(result);
      })
      .catch(error => {
        // setError(error.message);
      });
  }, [])

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <h1 className='message-prices-services'>Below you can see the services we offer in our garage</h1>
      {services && services.map((service) => {
        return ( 
          <Accordion align="center">
            <AccordionSummary
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography align={'center'} className={classes.heading} fontWeight='Bold'>{service.type}</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                {service.description}
              </Typography>
            </AccordionDetails>
          </Accordion>
        )
      }) 
      }
      
    </div>
  );

}
export default Services;