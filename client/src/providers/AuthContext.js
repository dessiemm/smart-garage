import { createContext } from 'react';
import jwtDecode from 'jwt-decode';

const AuthContext = createContext({
  isLoggedIn: false,
  customer: null,
  setAuthState: () => {},
}); 

export const getCustomer = () => {
  try {
    return jwtDecode(localStorage.getItem('token') || '');
  } catch (error) {
    return null;
  }
};

export default AuthContext;