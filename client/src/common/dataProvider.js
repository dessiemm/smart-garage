/* eslint-disable import/no-anonymous-default-export */
import queryString from 'querystring';

export const getCustomers = async (query) => {
    const qs = queryString.stringify(query);
    let url = `http://localhost:5555/customers${query ? '?' + qs : ''}`;

    try {
        if (url) {
            const response = await fetch(url);
            if (!response.ok) {
                throw new Error(response.statusText);
            };
            const result = await response.json();
            return result;
        };
    } catch (err) {
        console.log(err);
        return [];
    }
};

export const getServices = async (query) => {
    const qs = queryString.stringify(query);
    let url = `http://localhost:5555/services${query ? '?' + qs : ''}`;

    try {
        if (url) {
            const response = await fetch(url);
            if (!response.ok) {
                throw new Error(response.statusText);
            };
            const result = await response.json();
            return result;
        }
    } catch (err) {
        console.log(err);
        return [];
    }
};

export const getCars = async (query) => {
    const qs = queryString.stringify(query);
    console.log(qs, 'dati');
    let url = `http://localhost:5555/cars${query ? '?' + qs : ''}`;

    try {
        if (url) {
            const response = await fetch(url);
            if (!response.ok) {
                throw new Error(response.statusText);
            };
            const result = await response.json();
            return result;
        }
    } catch (err) {
        console.log(err);
        return [];
    }
};

export const getOrders = async (query) => {
    const qs = queryString.stringify(query);
    let url = `http://localhost:5555/orders${query ? '?' + qs : ''}`;

    try {
        if (url) {
            const response = await fetch(url);
            if (!response.ok) {
                throw new Error(response.statusText);
            };
            const result = await response.json();
            return result;
        }
    } catch (err) {
        console.log(err);
        return [];
    }
};

export const getReport = async (query) => {
    const qs = queryString.stringify(query);
    let url = `http://localhost:5555/customers/report${query ? '?' + qs : ''}`;
    console.log(url, 'reportURL')
    try {
        if (url) {
            console.log('if try catch')

            const response = await fetch(url);
            if (!response.ok) {
                throw new Error(response.statusText);
            };
            const result = await response.json();
            return result;
        }
    } catch (err) {
        console.log(err);
        return [];
    }
};
export const getReservations = async (query) => {
    const qs = queryString.stringify(query);
    let url = `http://localhost:5555/reservations${query ? '?' + qs : ''}`;
    console.log(url, 'reportURL')
    try {
        if (url) {
            console.log('if try catch')

            const response = await fetch(url);
            if (!response.ok) {
                throw new Error(response.statusText);
            };
            const result = await response.json();
            return result;
        }
    } catch (err) {
        console.log(err);
        return [];
    }
};