import React, { useContext } from 'react';
import AuthContext, { getCustomer } from '../providers/AuthContext';
import {
  Nav,
  NavLink,
  Bars,
  NavMenu,
  NavBtn,
  NavBtnLink
} from './NavBar-design';
import logourl from "../newlogo.jpg"
import SignIn from '../components/Signin';

const NavBar = () => {
  const auth = useContext(AuthContext);

const signOut = () => {
    localStorage.removeItem("token");
    auth.setAuthState({
      isLoggedIn: false,
      user: null,
    });
  }
  return (
    <>
      <Nav>
        <div className='logo'>
           <img src={logourl} alt='logo' width='92' height='72' /> 
        </div>
        <Bars />
        <NavMenu>
          <NavLink to='/home'>
            Home
          </NavLink>
          <NavLink to='/services'>
            Services
          </NavLink>
          <NavLink to='/servicesbook'>
            Book a service
          </NavLink>
          {auth.isLoggedIn && <NavLink to='/order'>
            Order Status
          </NavLink>}
          <NavLink to='/register'>
            Register
          </NavLink>
         {auth.isLoggedIn ? <NavBtnLink onClick={signOut} to='/signout'>Sign out</NavBtnLink> : <NavLink  to='/signin'>Sign In</NavLink>} 

           {/* <NavBtnLink to='/signin'>Sign In</NavBtnLink> */}
        </NavMenu>
        {/* <NavBtn>
        </NavBtn> */}
      </Nav>
    </>
  );
};

export default NavBar;